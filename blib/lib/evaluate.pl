
=head1 DESCRIPTION

Эта функция должна принять на вход ссылку на массив, который представляет из себя обратную польскую нотацию,
а на выходе вернуть вычисленное выражение

=cut

use 5.010;
use strict;
use warnings;
use diagnostics;

BEGIN {
    if ( $] < 5.018 ) {

        package experimental;
        use warnings::register;
    }
}
no warnings 'experimental';

sub evaluate {
    my $rpn   = shift;
    my @stack = ();
    my @token = @{$rpn};
    my $res;
    for (@token) {
        if ( $_ eq "U-" ) {
                my $x = pop(@stack);
                push( @stack, -$x );
            }
        elsif ( $_ eq "U+" ) {
            my $x = pop(@stack);
            push( @stack, $x );
        }
        elsif ( $_ =~ /[\+\-\*\/\^]/ ) {
            if ( scalar(@stack) < 2 ) {
                die 'NaN';
                exit;
            }
            my $x = pop(@stack);
            my $y = pop(@stack);
            if ( $_ eq '*' ) {
                    $res = $y * $x;
                }
            elsif ( $_ eq '/' ) {
                $res = $y / $x;
            }
            elsif ( $_ eq '+' ) {
                $res = $y + $x;
            }
            elsif ( $_ eq '-' ) {
                $res = $y - $x;
            }
            elsif ( $_ eq '^' ) {
                $res = $y ** $x;
            }
            else {
                die 'NaN';
                exit;
            }

            push( @stack, $res );
        }
        elsif ( $_ =~ /[0-9]/ ) {
            push( @stack, $_ );
        }
        else {
            die 'NaN';
            exit;
        }
    }
    if (@stack > 1) {
        die 'NaN';
        exit;
    }
    return pop(@stack);
}

1;
